# schelude_work

#### 介绍
spring5.0.3搭建的aop工作环境，通过CountDownLatch实现三个类似线程的同时启动同时结束

#### 软件架构
gradle构建环境，spring5.0.3+mybatis实现定时任务与数据库连接


#### 安装教程

1. 首先在eclipse上安装gradle插件
2. git上下载项目后刷新gradle项目
3. 找到Main.java的main方法运行程序

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)