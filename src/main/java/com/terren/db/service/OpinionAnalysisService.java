package com.terren.db.service;

import java.util.List;

import com.terren.common.entity.Article;
import com.terren.common.entity.WxArticle;
import com.terren.common.entity.ZhArticle;
import com.terren.db.basic.IBasicServiceSupport;

public interface OpinionAnalysisService extends IBasicServiceSupport{
	public void saveArticles(List<Article> items) throws Exception;
	public void saveWxArticles(List<WxArticle> items) throws Exception;
	public void saveZhArticles(List<ZhArticle> items) throws Exception;
	public List<String> getKeywords() throws Exception;
}
