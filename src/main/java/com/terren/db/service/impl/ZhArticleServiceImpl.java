package com.terren.db.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.terren.common.entity.ZhArticle;
import com.terren.db.basic.BasicServiceSupportImpl;
import com.terren.db.service.ZhArticleService;

@Service("zhArticleService")
public class ZhArticleServiceImpl extends BasicServiceSupportImpl implements ZhArticleService{
	private final String SQL_MAPPER = "zhArticleMapper.";

	@Override
	public List<ZhArticle> selectList(ZhArticle article) throws Exception {
		return basicDao.findForList(SQL_MAPPER+"getArticleList", article);
	}

	@Override
	public void updateDoneStatus(List<ZhArticle> items) throws Exception {
		basicDao.save(SQL_MAPPER + "updateDoneStatus", items);
	}

	@Override
	public List<String> getKeywords(ZhArticle article) throws Exception {
		return basicDao.findForList(SQL_MAPPER+"getKeywords", article);
	}

	@Override
	public void updateDoingStatus(List<ZhArticle> items) throws Exception {
		basicDao.save(SQL_MAPPER + "updateDoingStatus", items);
	}

	@Override//如果存在一个事务，则支持当前事务。如果没有事务则开启一个新的事务
	@Transactional(propagation= Propagation.REQUIRED,rollbackFor= {Exception.class, RuntimeException.class})
	public List<ZhArticle> getArticleList(ZhArticle article) throws Exception {
		List<ZhArticle> list = selectList(article);
		if(CollectionUtils.isNotEmpty(list)) {
			this.updateDoingStatus(list);
		}
		return list;
	}
}