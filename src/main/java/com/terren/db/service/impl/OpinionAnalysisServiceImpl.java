package com.terren.db.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.terren.common.entity.Article;
import com.terren.common.entity.OpinionAnalysis;
import com.terren.common.entity.WxArticle;
import com.terren.common.entity.ZhArticle;
import com.terren.db.basic.BasicServiceSupportImpl;
import com.terren.db.service.OpinionAnalysisService;
@Service("opinionAnalysisService")
public class OpinionAnalysisServiceImpl extends BasicServiceSupportImpl implements OpinionAnalysisService{
	private final String SQL_MAPPER = "opinionAnalysisMapper.";
	@Override
	public void saveArticles(List<Article> items) throws Exception {
		List<OpinionAnalysis> list = new ArrayList<OpinionAnalysis>();
		for(Article a : items) {
			OpinionAnalysis o = new OpinionAnalysis();
			o.setUrlid(a.getUrlid());
			o.setUrl(a.getUrl());
			o.setTitle(a.getTitle());
			o.setSummary(a.getSummary());
			o.setContent(a.getContent());
			o.setKeyword(a.getKeyword());
			if(StringUtils.isNotBlank(a.getAuthor())) {
				o.setAuthor(a.getAuthor());
			}else {
				o.setAuthor("-");
			}
			if(StringUtils.isNotBlank(a.getPubTime())) {
				o.setPubtime(a.getPubTime());
			}else {
				o.setPubtime("0000-00-00");
			}	
			o.setNewssource(a.getNewsSource());
			o.setOpinion(a.getOpinion_analysis());
			list.add(o);
		}
		basicDao.save(SQL_MAPPER+"saveOpinionAnalysis", list);	
	}

	@Override
	public void saveWxArticles(List<WxArticle> items) throws Exception {
		List<OpinionAnalysis> list = new ArrayList<OpinionAnalysis>();
		for(WxArticle a : items) {
			OpinionAnalysis o = new OpinionAnalysis();
			o.setUrlid(a.getUrlid());
			o.setUrl(a.getUrl());
			o.setTitle(a.getTitle());
			o.setSummary("-");
			o.setContent(a.getContent());
			o.setKeyword(a.getKeyword());
			if(StringUtils.isNotBlank(a.getAuthor())) {
				o.setAuthor(a.getAuthor());
			}else {
				o.setAuthor("-");
			}
			if(StringUtils.isNotBlank(a.getPubTime())) {
				o.setPubtime(a.getPubTime());
			}else {
				o.setPubtime("0000-00-00");
			}	
			o.setNewssource(a.getNewsSource());
			o.setOpinion(a.getOpinion_analysis());
			list.add(o);
		}
		basicDao.save(SQL_MAPPER+"saveOpinionAnalysis", list);
		
	}

	@Override
	public void saveZhArticles(List<ZhArticle> items) throws Exception {
		List<OpinionAnalysis> list = new ArrayList<OpinionAnalysis>();
		for(ZhArticle a : items) {
			OpinionAnalysis o = new OpinionAnalysis();
			o.setUrlid(a.getUrlid());
			o.setUrl(a.getUrl());
			o.setTitle(a.getTitle());
			o.setSummary("-");
			o.setContent(a.getTopicContent());
			o.setKeyword(a.getKeyword());
			if(StringUtils.isNotBlank(a.getAuthorName())) {
				o.setAuthor(a.getAuthorName());
			}else {
				o.setAuthor("-");
			}
			if(StringUtils.isNotBlank(a.getPubTime())) {
				o.setPubtime(a.getPubTime());
			}else {
				o.setPubtime("0000-00-00");
			}	
			o.setNewssource("-");
			o.setOpinion(a.getOpinion_analysis());
			list.add(o);
		}
		basicDao.save(SQL_MAPPER+"saveOpinionAnalysis", list);
		
	}

	@Override
	public List<String> getKeywords() throws Exception {
		return basicDao.findForList(SQL_MAPPER+"getKeywords", null);
	}

}
