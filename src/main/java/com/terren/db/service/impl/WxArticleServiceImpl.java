package com.terren.db.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.terren.common.entity.WxArticle;
import com.terren.db.basic.BasicServiceSupportImpl;
import com.terren.db.service.WxArticleService;

@Service("wxArticleService")
public class WxArticleServiceImpl extends BasicServiceSupportImpl implements WxArticleService{
	private final String SQL_MAPPER = "wxArticleMapper.";

	@Override
	public List<WxArticle> selectList(WxArticle article) throws Exception {
		return basicDao.findForList(SQL_MAPPER+"getArticleList", article);
	}

	@Override
	public void updateDoneStatus(List<WxArticle> items) throws Exception {
		basicDao.save(SQL_MAPPER + "updateDoneStatus", items);
	}

	@Override
	public List<String> getKeywords(WxArticle article) throws Exception {
		return basicDao.findForList(SQL_MAPPER+"getKeywords", article);
	}

	@Override
	public void updateDoingStatus(List<WxArticle> items) throws Exception {
		basicDao.save(SQL_MAPPER + "updateDoingStatus", items);
	}

	@Override//如果存在一个事务，则支持当前事务。如果没有事务则开启一个新的事务
	@Transactional(propagation= Propagation.REQUIRED,rollbackFor= {Exception.class, RuntimeException.class})
	public List<WxArticle> getArticleList(WxArticle article) throws Exception {
		List<WxArticle> list = selectList(article);
		if(CollectionUtils.isNotEmpty(list)) {
			this.updateDoingStatus(list);
		}
		return list;
	}
	
}