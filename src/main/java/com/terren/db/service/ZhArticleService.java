package com.terren.db.service;

import java.util.List;

import com.terren.common.entity.ZhArticle;
import com.terren.db.basic.IBasicServiceSupport;

public interface ZhArticleService extends IBasicServiceSupport{
	public List<ZhArticle> selectList(ZhArticle article) throws Exception;
	public void updateDoneStatus(List<ZhArticle> items) throws Exception;
	public List<String> getKeywords(ZhArticle article) throws Exception;
	public void updateDoingStatus(List<ZhArticle> items) throws Exception;
	public List<ZhArticle> getArticleList(ZhArticle article) throws Exception;
}
