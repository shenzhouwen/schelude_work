package com.terren.db.service;

import java.util.List;

import com.terren.common.entity.WxArticle;
import com.terren.db.basic.IBasicServiceSupport;

public interface WxArticleService extends IBasicServiceSupport{
	public List<WxArticle> selectList(WxArticle article) throws Exception;
	public void updateDoneStatus(List<WxArticle> items) throws Exception;
	public List<String> getKeywords(WxArticle article) throws Exception;
	public void updateDoingStatus(List<WxArticle> items) throws Exception;
	public List<WxArticle> getArticleList(WxArticle article) throws Exception;
}
