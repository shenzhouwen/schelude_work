package com.terren.db.service;

import java.util.List;

import com.terren.common.entity.Article;
import com.terren.db.basic.IBasicServiceSupport;

public interface ArticleService extends IBasicServiceSupport{
	public List<Article> selectList(Article article) throws Exception;
	public void updateDoneStatus(List<Article> items) throws Exception;
	public List<String> getKeywords(Article article) throws Exception;
	public void updateDoingStatus(List<Article> items) throws Exception;
	
	public List<Article> getArticleList(Article article) throws Exception;
}
