package com.terren.db.basic;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;



/**
 * 主要的 dao 类
 * 
 * @author Administrator
 * 
 */
@Repository("basicDaoSupport")
public class BasicDaoSupportImpl extends SqlSessionDaoSupport implements IBasicDaoSupport {

	/**
	 * mybatis-spring-1.2.x.jar 版本的 sqlSessionTemplate 注入有所改动，必须重写次方法
	 */
	@Autowired
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
		super.setSqlSessionTemplate(sqlSessionTemplate);
	}

	protected <S> S getMapper(Class<S> clazz) {
		return getSqlSession().getMapper(clazz);
	}

	public Object delete(String sql, Object obj) throws Exception {
		logger.info("delete["+sql+"]");
		SqlSession session = null;
		Object object = null;
		try {
			session = this.getSqlSession();
			object = session.delete(sql, obj);
		} catch (Exception e) {
			throw e;
		} 
		return object;
	}


	public <T> List<T> findForList(String sql, Object obj) throws Exception {
		logger.info("findForList["+sql+"]");
		SqlSession session = null;
		List<T> object = null;
		try {
			session = this.getSqlSession();
			object = session.selectList(sql, obj);
		} catch (Exception e) {
			throw e;
		} 
		return object;
	}

	public Object findForMap(String sql, Object obj, String key, String value)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T findForObject(String sql, Object obj) throws Exception {
		logger.info("findForObject["+sql+"]");
		// TODO Auto-generated method stub
		SqlSession session = null;
		T object = null;
		try {
			session = this.getSqlSession();
			object = session.selectOne(sql, obj);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		} 
		return object;
	}


	public Object save(String sql, Object paramObj) throws Exception {
		logger.info("save["+sql+"]");
		SqlSession session = null;
		Object object = null;
		try {
			session = this.getSqlSession();
			object = session.insert(sql, paramObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}
	
	public int saveList(String sql, List<?> list) throws Exception {
		int count = 0;
		try {
			for(Object entity : list) {
				count += getSqlSession().insert(sql, entity);
			}
		} catch (Exception e) {
			e.printStackTrace();
			count = -1;
		}
		return count;
	}



	
}
