package com.terren.db.basic;

import java.util.List;

/**
 * 数据库相应操作
 * @author Administrator
 *
 */
public interface IBasicDaoSupport {
	/**
	 * 保存对象
	 * @param sql
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Object save(String sql, Object obj) throws Exception;
	
	public int saveList(String sql, List<?> list) throws Exception;



	
	public Object delete(String sql, Object obj) throws Exception;


	/**
	 * 查找对象
	 * @param sql
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public <T> T findForObject(String sql, Object obj) throws Exception;

	/**
	 * 查找对象集合
	 * @param sql
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public <T> List<T> findForList(String sql, Object obj) throws Exception;

	/**
	 * 查找对象封装成Map
	 * 
	 * @param s
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Object findForMap(String sql, Object obj, String key, String value)
			throws Exception;


}