package com.terren.config;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

import com.terren.common.util.CommonUtil;
import com.terren.common.util.LogUtil;

public class SysConfig {
	public static Boolean SCHEDULED_RUN = false;//是否运行定时任务
	public static String PROJECT_PATH;
	public static String LOG4J_PATH;
	public static Integer LIMIT;//sql查询每次查询的条目数
	public static String RPC_URL;
	//public static String[] KEYWORD = {"借点钱","信用算力","鑫涌算力","融之家","投之家","张建梁"};
	
	static {
		PROJECT_PATH = CommonUtil.getProjectPath(SysConfig.class)+File.separator;
		LOG4J_PATH =  PROJECT_PATH+"config"+File.separator+"log4j"+File.separator+"log4j.xml";
		
		/**
		 * 初始化配置文件内容
		 */
		Properties properties = getSysConfigProperties();
		/***** 读取配置文件 *****/
		String limit = properties.getProperty("limit");
		if (null != limit) {
			LIMIT = Integer.parseInt(limit);
		}
		String rpcUrl = properties.getProperty("rpc.url");
		if (null != rpcUrl) {
			RPC_URL = rpcUrl;
		}
	}
	
	
	

	/**
	 * 读取配置文件内容
	 */
	private static Properties getSysConfigProperties() {
		InputStream resourceAsStream = null;
		String configPath = "config" + File.separator + "sysConfig.properties";
		resourceAsStream = CommonUtil.class.getClassLoader().getResourceAsStream(configPath);
		if (resourceAsStream == null) {
            LogUtil.logger.error("没有找到配置文件:"+configPath);
            return null;
        }
        Properties properties = new Properties();
        try {
            properties.load(resourceAsStream);
        }catch (Exception e) {
        	LogUtil.logger.error("读取配置文件错误:"+"config" + File.separator + "sysConfig.properties",e);
		}
        return properties;
	}
}
