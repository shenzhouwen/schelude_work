package com.terren.common.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * 自定义定时任务配置 
 * 涉及的接口：ITaskSchedulingHandler
 * 
 * 
 * 使用自定义的ThreadPoolTaskScheduler：CustomThreadPoolTaskScheduler
 * poolsize：20
 * 守护线程：true
 * 
 *
 */

@Configuration
public class CustomSchedulingConfigurer implements SchedulingConfigurer {

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        ThreadPoolTaskScheduler taskScheduler = new CustomThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(20);
       // taskScheduler.setDaemon(true);
        taskScheduler.initialize();
        taskRegistrar.setTaskScheduler(taskScheduler);
    }
}
