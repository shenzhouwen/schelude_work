package com.terren.common.interceptor;

import java.util.Date;
import java.util.concurrent.ScheduledFuture;

import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.terren.config.SysConfig;

public class CustomThreadPoolTaskScheduler extends ThreadPoolTaskScheduler {
    /**
	 * 
	 */
	private static final long serialVersionUID = -8987774261009749319L;
	@Override
	public ScheduledFuture<?> schedule(Runnable task, Trigger trigger) {
		if (SysConfig.SCHEDULED_RUN) {
			ScheduledFuture<?> future = super.schedule(task, trigger);
			return future;
		}
		return null;
	}
	
	
	@Override
    public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long period) {
        if (SysConfig.SCHEDULED_RUN) {
			ScheduledFuture<?> future = super.scheduleAtFixedRate(task, period);
			return future;
		}
        return null;
    }

    @Override
    public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, Date startTime, long period) {
    	if (SysConfig.SCHEDULED_RUN) {
			ScheduledFuture<?> future = super.scheduleAtFixedRate(task,startTime, period);
			return future;
		}
        return null;
    }
}