package com.terren.common.entity;

public class WxArticle  extends BasicEntity{
	
	private static final long serialVersionUID = 2108551888302194746L;

	private Integer sourceId; // 网站来源ID
	private Integer catagoryId; // 网站类型ID
	private Integer entryId; // 入口ID
	private Integer index; // 序号
	private String title;
	private String pubTime;
	private String author;
	private String userId;
	private Long wx_mid;
	private Integer wx_idx;
	private String content;
	private String html; //网页所有内容
	private String newsSource;
	private String hashId;
	private Long taskId;
	private Integer occurance = 0;//关键词出现次数
	
	
	public Integer getSourceId() {
		return sourceId;
	}
	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}
	public Integer getCatagoryId() {
		return catagoryId;
	}
	public void setCatagoryId(Integer catagoryId) {
		this.catagoryId = catagoryId;
	}
	public Integer getEntryId() {
		return entryId;
	}
	public void setEntryId(Integer entryId) {
		this.entryId = entryId;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public void setWx_mid(Long wx_mid) {
		this.wx_mid = wx_mid;
	}
	public void setWx_idx(Integer wx_idx) {
		this.wx_idx = wx_idx;
	}
	public void setHashId(String hashId) {
		this.hashId = hashId;
	}
	public Integer getOccurance() {
		return occurance;
	}
	public void setOccurance(Integer occurance) {
		this.occurance = occurance;
	}
	public String getHashId() {
		return hashId;
	}


	public Long getTaskId() {
		return taskId;
	}


	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}


	public String getNewsSource() {
		return newsSource;
	}
	public void setNewsSource(String newsSource) {
		this.newsSource = newsSource;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPubTime() {
		return pubTime;
	}
	public void setPubTime(String pubTime) {
		this.pubTime = pubTime;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public long getWx_mid() {
		return wx_mid;
	}
	public void setWx_mid(long wx_mid) {
		this.wx_mid = wx_mid;
	}
	public int getWx_idx() {
		return wx_idx;
	}
	public void setWx_idx(int wx_idx) {
		this.wx_idx = wx_idx;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

}
