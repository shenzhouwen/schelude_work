package com.terren.common.entity;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigInteger;




public class BasicEntity implements Cloneable,Serializable{
	private BigInteger urlid; // URL的64位哈希
	private String url = "";// URL地址
	private Integer limit;
	private int opinion_analysis;//舆情指数分析 0:未分析 1:已分析 -1:分析错误
	private String keyword;
	
	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getOpinion_analysis() {
		return opinion_analysis;
	}

	public void setOpinion_analysis(int opinion_analysis) {
		this.opinion_analysis = opinion_analysis;
	}
	
	public BigInteger getUrlid() {
		return urlid;
	}

	public void setUrlid(BigInteger urlid) {
		this.urlid = urlid;
	}
	
	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5422192679383017903L;
	/**
	 * 数据库连接类型
	 */
	protected String dbType;


	public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	/**
	 * 
	 */
	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        Field[] fields = getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                sb.append(field.getName()).append(":").append(field.get(this)).append(",");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // 去掉多于的逗号
        if(sb.length() > 1){
            sb.deleteCharAt(sb.length()-1);
        }
        sb.append("]");
        return sb.toString();
    }

	@Override
	public Object clone() {   
        try {   
            return super.clone();   
        } catch (CloneNotSupportedException e) {   
            return null;   
        }   
    }   
}
