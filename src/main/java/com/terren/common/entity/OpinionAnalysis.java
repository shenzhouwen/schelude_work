package com.terren.common.entity;

public class OpinionAnalysis extends BasicEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5063696739378797407L;
	
	private String title = ""; // 文章标题
	private String summary = ""; // 文章摘要
	private String content = ""; // 文章内容
	private String author = ""; // 文章发布作者
	private String pubtime = "0000-00-00"; // 文章的发布时间
	private String newssource = ""; // 文章的新闻来源
	private int opinion;//舆情指数分析 0:未分析 1:已分析 -1:分析错误
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPubtime() {
		return pubtime;
	}
	public void setPubtime(String pubtime) {
		this.pubtime = pubtime;
	}
	public String getNewssource() {
		return newssource;
	}
	public void setNewssource(String newssource) {
		this.newssource = newssource;
	}
	public int getOpinion() {
		return opinion;
	}
	public void setOpinion(int opinion) {
		this.opinion = opinion;
	}
	
	
}
