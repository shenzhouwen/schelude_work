package com.terren.common.entity;

public class Article extends BasicEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5063696739378797407L;
	
	
	
	private Integer sourceId; // 网站来源ID
	private Integer catagoryId; // 网站类型ID
	private Integer entryId; // 入口ID
	private Integer index; // 序号


	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getCatagoryId() {
		return catagoryId;
	}

	public void setCatagoryId(Integer catagoryId) {
		this.catagoryId = catagoryId;
	}

	public Integer getEntryId() {
		return entryId;
	}

	public void setEntryId(Integer entryId) {
		this.entryId = entryId;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	private String newsId = ""; // 文章ID
	private String title = ""; // 文章标题
	private String summary = ""; // 文章摘要
	private String content = ""; // 文章内容
	
	private String author = ""; // 文章发布作者
	private String pubTime = "0000-00-00"; // 文章的发布时间
	private String newsSource = ""; // 文章的新闻来源
	private String keyword = ""; // 文章中出现的关键词
	private Integer clickCount = 0; // 文章的点击数或者阅读数
	private Short hot = 0; // 文章的关注程度
	private Integer replyCount = 0; // 文章的评论数
	private Short negative = 0; // 正负面标识,0：非负面，1：负面，2：预警
	private Long fingerPrint = 0l; // 文本指纹
	private Short ml_catagorypid = 0; // 内容一级分类
	private String parentCategory = ""; // 内容一级分类名称
	private Short ml_catagoryid = 0; // 内容二级分类
	private String category = ""; // 内容二级分类名称
	private String entity = ""; // 实体词
	private Short filter = 0; // 是否为娱乐类内容,0：非，1：是，2：未判定
	private Short correlation = 0; // 文章相关性,0-1000之间
	private Short riskDegree = 0; // 文章风险度,0-1000之间
	private Integer month = 0; // 年月

	private String html; // 网页所有内容
	private String contentHtml;//网页正文html
	
	//旧版爬虫字段
	private String editor;//作者——author
	private String article;//文章-content
	private Integer click;//点击次数-clickcount
	private String source;//来源-newsSource
	

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public Integer getClick() {
		return click;
	}

	public void setClick(Integer click) {
		this.click = click;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}


	public String getContentHtml() {
		return contentHtml;
	}

	public void setContentHtml(String contentHtml) {
		this.contentHtml = contentHtml;
	}

	public String getNewsId() {
		return newsId;
	}

	public void setNewsId(String newsId) {
		this.newsId = newsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}


	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPubTime() {
		return pubTime;
	}

	public void setPubTime(String pubTime) {
		this.pubTime = pubTime;
	}

	public String getNewsSource() {
		return newsSource;
	}

	public void setNewsSource(String newsSource) {
		this.newsSource = newsSource;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getClickCount() {
		return clickCount;
	}

	public void setClickCount(Integer clickCount) {
		this.clickCount = clickCount;
	}

	public Short getHot() {
		return hot;
	}

	public void setHot(Short hot) {
		this.hot = hot;
	}

	public Integer getReplyCount() {
		return replyCount;
	}

	public void setReplyCount(Integer replyCount) {
		this.replyCount = replyCount;
	}

	public Short getNegative() {
		return negative;
	}

	public void setNegative(Short negative) {
		this.negative = negative;
	}
	

	public Long getFingerPrint() {
		return fingerPrint;
	}

	public void setFingerPrint(Long fingerPrint) {
		this.fingerPrint = fingerPrint;
	}

	public Short getMl_catagorypid() {
		return ml_catagorypid;
	}

	public void setMl_catagorypid(Short ml_catagorypid) {
		this.ml_catagorypid = ml_catagorypid;
	}

	public Short getMl_catagoryid() {
		return ml_catagoryid;
	}

	public String getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setMl_catagoryid(Short ml_catagoryid) {
		this.ml_catagoryid = ml_catagoryid;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public Short getFilter() {
		return filter;
	}

	public void setFilter(Short filter) {
		this.filter = filter;
	}

	public Short getCorrelation() {
		return correlation;
	}

	public void setCorrelation(Short correlation) {
		this.correlation = correlation;
	}

	public Short getRiskDegree() {
		return riskDegree;
	}

	public void setRiskDegree(Short riskDegree) {
		this.riskDegree = riskDegree;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}
}
