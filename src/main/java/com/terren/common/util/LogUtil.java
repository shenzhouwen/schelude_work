package com.terren.common.util;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.terren.config.SysConfig;
/**
 * 常用工具类
 * 
 * @author Administrator
 *
 */
public class LogUtil {

	public static Logger logger = getLogger(LogUtil.class);

	public static void initLogger() {
		DOMConfigurator.configure(SysConfig.LOG4J_PATH);
	}

	/**
	 * 获得log4j logger 对象
	 * 
	 * @param clazz
	 * @return
	 */
	public static Logger getLogger(Class<?> clazz) {
		Logger logger = Logger.getLogger(clazz);
		return logger;
	}

}
