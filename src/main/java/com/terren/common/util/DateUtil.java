package com.terren.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;


/**
 * 时间工具类
 * 
 * @author Administrator
 * 
 */
public class DateUtil {

	/**
	 * 毫秒时间戳 + 随机数
	 * 
	 * @return string 时间戳
	 */
	public static String getNanoRandom() {
		long time = System.nanoTime();
		Random random = new Random();
		time += random.nextInt(999999999);
		return String.valueOf(time);
	}

	/**
	 * 得到当前时间时间
	 * 
	 * @param 时间格式
	 * @return string 按格式返回当前时间
	 */
	public static int getCurrentDay() {
		long time = System.currentTimeMillis();
		Date date = new Date(time);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		int day = Integer.parseInt(sdf.format(date));
		return day;
	}

	/**
	 * 获得当前日期
	 * 
	 * @return
	 */
	public static String getNowTime(String formatType) {
		Date date = new Date();

		SimpleDateFormat format = new SimpleDateFormat(formatType);

		String resultDay = format.format(date);

		return resultDay;
	}

	/**
	 * 将 2014-01-11 这样的日期转换成 20140211
	 * 
	 * @param day
	 * @return
	 */
	public static long parseDayToLong(String day) {
		long longDay = new Long(day.replaceAll("-", ""));

		return longDay;
	}

	/**
	 * 将日期转成成字符串
	 * 
	 * @param date
	 * @param formatType
	 * @return
	 */
	public static String dateToString(Date date, String formatType) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(formatType);
		String resultDate = dateFormat.format(date);
		return resultDate;
	}

	/**
	 * 字符串转日期
	 * 
	 * @param dateString
	 * @param formatType
	 * @return
	 */
	public static Date stringToDate(String dateString, String formatType) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(formatType);
		Date resultDate = null;
		try {
			resultDate = dateFormat.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return resultDate;
	}

	/**
	 * 截取日期月份
	 * 
	 * @param 日期格式
	 *            2012-09-07
	 * @return string 1209
	 */
	public static String dateForMonth(String date) {
		if (date == null) {
			return null;
		}
		String date_sub = date.replaceAll("-", "");
		return date_sub.substring(2, 6);
	}

	/**
	 * 转换日期格式
	 * 
	 * @param dateString
	 * @param oldFormat
	 * @param newFormat
	 * @return
	 */
	public static String formatDateString(String dateString, String oldFormat, String newFormat) {
		Date oldDate = stringToDate(dateString, oldFormat);
		String newDateString = dateToString(oldDate, newFormat);
		return newDateString;
	}

	/**
	 * 获得两个月份段之间的所有月份
	 * 
	 * @param startMonth
	 * @param endMonth
	 * @return
	 */
	public static List<Integer> getBetweenMonth(String startMonth, String endMonth) {
		List<Integer> result = null;

		if (startMonth.length() == 4 && endMonth.length() == 4) {
			int startInt = Integer.parseInt(startMonth);
			int endInt = Integer.parseInt(endMonth);

			if (endInt >= startInt) {
				result = new ArrayList<Integer>();

				int i = startInt;

				result.add(i);

				while (i < endInt) {
					int month = i % 100;

					if (month >= 12) {// 如果到 12月了
						i = i / 100 * 100 + 101;
					} else {
						i++;
					}

					result.add(i);
				}
			}
		}
		return result;
	}

	/**
	 * 毫秒转换成时间格式
	 * 
	 * @param timeMillis
	 * @return
	 */
	public static String timeMillisToString(Long timeMillis) {
		Date date = new Date(timeMillis);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);
	}

	/**
	 * 得到当前日期所在的周
	 * 
	 * @param date
	 * @return
	 */
	public static int getWeek(String start) {
		Calendar cal = Calendar.getInstance();
		try {
			Date date = DateUtils.parseDate(start, "yyyy-MM-dd");
			cal.setTime(DateUtils.addDays(date, -1));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int week = cal.get(Calendar.DAY_OF_WEEK);
		return week;
	}

	public static int getWeek(Date start) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(DateUtils.addDays(start, -1));
		int week = cal.get(Calendar.DAY_OF_WEEK);
		return week;
	}

	/**
	 * 返回从开始时间到结束时间的星期，以逗号隔开（如：1,2,3,4）
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static String getDaysBetweenWeeks(String start, String end) {
		Integer days = daysBetween(start, end);
		if (days <= 0) {
			return String.valueOf(getWeek(start));
		}
		if (days >= 7) {
			return "1,2,3,4,5,6,7";
		}
		if (days < 7 && days > 0) {
			try {
				String weeks = "";
				Date date = DateUtils.parseDate(start, "yyyy-MM-dd");
				for (int i = 0; i <= days; i++) {
					if (i < days) {
						weeks += String.valueOf(getWeek(DateUtils.addDays(date, i))) + ",";
					} else {
						weeks += String.valueOf(getWeek(DateUtils.addDays(date, i)));
					}
				}
				return weeks;
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return String.valueOf(getWeek(start));
	}

	/**
	 * 计算开始时间和结束时间相差的天数
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static Integer daysBetween(String start, String end) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(sdf.parse(start));
			long time1 = cal.getTimeInMillis();
			cal.setTime(sdf.parse(end));
			long time2 = cal.getTimeInMillis();
			long between_days = (time2 - time1) / (1000 * 3600 * 24);
			return Integer.parseInt(String.valueOf(between_days));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获得年月
	 * 
	 * @return
	 */
	public static int getYearMonth(String dateString, String formatType) {
		String newFormat = "yyyyMMdd";
		String knownDate = formatDateString(dateString, formatType, newFormat);

		int yearMonth = Integer.parseInt(knownDate.substring(2, 6));
		return yearMonth;
	}

	/**
	 * 
	 * @param 要转换的毫秒数
	 * @return 该毫秒数转换为 * days * hours * minutes * seconds 后的格式
	 * @author fy.zhang
	 */
	public static String formatDuring(long mss) {
		long days = mss / (1000 * 60 * 60 * 24);
		long hours = (mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
		long minutes = (mss % (1000 * 60 * 60)) / (1000 * 60);
		long seconds = (mss % (1000 * 60)) / 1000;
		String result = "";
		if (days > 0) {
			result += days + " 天 " + hours + " 小时 " + minutes + " 分钟 " + seconds + " 秒 ";
		} else if (hours > 0) {
			result += hours + " 小时 " + minutes + " 分钟 " + seconds + " 秒 ";
		} else if (minutes > 0) {
			result += minutes + " 分钟 " + seconds + " 秒 ";
		} else {
			result += seconds + " 秒 ";
		}
		return result;
	}

	/**
	 * @param startDateStr
	 *            需要比较的时间 不能为空(null),需要正确的日期格式
	 * @param endDateStr
	 *            被比较的时间 为空(null)则为当前时间
	 * @param type
	 *            返回值类型 0为多少天，1为多少个月，2为多少年
	 * @return
	 */
	public static List<String> compareDate(String startDateStr, String endDateStr, int type) {
		List<String> dateList = new ArrayList<String>();

		// String[] typeArray = {"天", "月", "年"};

		String formatType = "yyyy-MM-dd";
		SimpleDateFormat dateFormat = new SimpleDateFormat(formatType);

		Date startDate = stringToDate(startDateStr, formatType);
		Date endDate = stringToDate(endDateStr, formatType);

		Calendar startCal = Calendar.getInstance();
		startCal.setTime(startDate);

		Calendar endCal = Calendar.getInstance();
		endCal.setTime(endDate);

		// 先将开始日期放进去
		String firstDateStr = dateFormat.format(startCal.getTime());
		dateList.add(firstDateStr);

		while (endCal.after(startCal)) { // 循环对比，直到相等，n 就是所要的结果
			if (type == 0) {// 日
				startCal.add(Calendar.DAY_OF_MONTH, 1);
			} else if (type == 1) {// 月
				startCal.add(Calendar.MONTH, 1);
			} else {// 年
				startCal.add(Calendar.YEAR, 1);
			}

			String oneDateStr = null;
			if (startCal.after(endCal)) {// 如果 开始时间增加后 大于 结束时间，则放入结束时间
				oneDateStr = dateFormat.format(endCal.getTime());
			} else {// 如果 开始时间增加后 大于 结束时间，则放入结束时间
				oneDateStr = dateFormat.format(startCal.getTime());
			}
			dateList.add(oneDateStr);
		}
		return dateList;
	}

	/**
	 * 时间戳
	 * 
	 * @return string 时间戳
	 */
	public static String getCurrentTimeMillis() {
		long time = System.currentTimeMillis();
		Date date = new Date(time);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return sdf.format(date);
	}

	/**
	 * 得到系统日期的n天前或n天后的日期 - String
	 */
	public static String getSubCurrentDate(int n) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, n);
		Date date = calendar.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateTime = sdf.format(date);
		return dateTime;
	}

	/**
	 * 得到当前时间时间
	 * 
	 * @param 时间格式
	 * @return string 按格式返回当前时间
	 */
	public static String getCurrentDataTime(String format) {
		long time = System.currentTimeMillis();
		Date date = new Date(time);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	/**
	 * 获得当前系统时间
	 * 
	 * @param formatType
	 *            返回的日期格式
	 * @return
	 */
	public static String getCurrentDate(String formatType) {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat(formatType);
		String currentDate = format.format(date);
		return currentDate;
	}

	/**
	 * 取得当前日期 前后n天的日期 n = -7 前7天 n = 7 后7天
	 */
	public static String subDate(String dateString, int n) {
		Calendar calendar = Calendar.getInstance();
		Date date_old = toDate(dateString, "yyyy-MM-dd");
		calendar.setTime(date_old);
		calendar.add(Calendar.DATE, n);
		Date date = calendar.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	/**
	 * 字符窜(yyyyMMdd)转换成为java.util.Date
	 * 
	 * @param sDate
	 *            字符串(yyyyMMdd)
	 * @param sFmt
	 *            format
	 * @return Date java.util.Date日期
	 */
	public static Date toDate(String sDate, String sFmt) {
		Date dt = null;
		try {
			dt = new SimpleDateFormat(sFmt).parse(sDate);
		} catch (ParseException e) {
			return dt;
		}
		return dt;
	}

	/**
	 * 两个日期之间相隔天数的共通
	 * 
	 * @param dateFrom
	 *            开始时间
	 * @param dateEnd
	 *            结束时间
	 * @return 天数
	 */
	public static int getDaysBetweenTwoDates(String dateFrom, String dateEnd) {
		Date dtFrom = toDate(dateFrom, "yyyy-MM-dd");
		Date dtEnd = toDate(dateEnd, "yyyy-MM-dd");

		long begin = dtFrom.getTime();
		long end = dtEnd.getTime();
		long inter = end - begin;
		if (inter < 0) {
			inter = inter * (-1);
		}
		long dateMillSec = 24 * 60 * 60 * 1000;

		Long dateCnt = inter / dateMillSec;

		long remainder = inter % dateMillSec;

		if (remainder != 0) {
			dateCnt++;
		}
		return dateCnt.intValue();
	}

	/**
	 * 取得当前日期 前后n月的日期 n = -7 前7个月 n = 7 后7个月
	 */
	public static String subMonth(String dateString, int n) {
		Calendar calendar = Calendar.getInstance();
		Date date_old = toDate(dateString, "yyyy-MM-dd");
		calendar.setTime(date_old);
		calendar.add(Calendar.MONTH, n);
		Date date = calendar.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	/**
	 * 取得当前日期 前后n小时的日期 n = -7 前7小时 n = 7 后7小时
	 */
	public static String newDateNewsForHour(int h, String format) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, h);
		Date date = calendar.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateTime = sdf.format(date);
		return dateTime;
	}

	/**
	 * 取得当前日期 前后n分钟的日期 m = -7 前7分钟 m = 7 后7分钟
	 */
	public static String newDateNewsForMinute(int m, String format) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, m);
		Date date = calendar.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateTime = sdf.format(date);
		return dateTime;
	}

	/**
	 * 取得XX日期 前后n小时的日期 n = -7 前7小时 n = 7 后7小时
	 */
	public static String subHour(String dateString, int n) {
		Calendar calendar = Calendar.getInstance();
		Date date_old = toDate(dateString, "yyyy-MM-dd:HH");
		calendar.setTime(date_old);
		calendar.add(Calendar.HOUR_OF_DAY, n);
		Date date = calendar.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd:HH");
		return sdf.format(date);
	}

	/**
	 * 把毫秒转换成时长，小时、分钟、秒
	 * 
	 * @param millis
	 * @param format
	 * @return
	 */
	public static String formatLongToTimeStr(Long millis, String format) {
		String str = "";
		long hour = 0;
		long minute = 0;
		long second = 0;

		second = millis / 1000;
		if (second > 60) {
			minute = second / 60;
			second = second % 60;
		}
		if (minute > 60) {
			hour = minute / 60;
			minute = minute % 60;
		}
		if (hour > 0) {
			str = StringUtils.replaceEach(format, new String[] { "HH", "mm", "ss" }, new String[] {
					String.format("%0$02d", hour), String.format("%0$02d", minute), String.format("%0$02d", second) });
		} else if (hour == 0 && minute > 0) {
			format = "mm" + StringUtils.substringAfter(format, "mm");
			str = StringUtils.replaceEach(format, new String[] { "mm", "ss" },
					new String[] { String.format("%0$02d", minute), String.format("%0$02d", second) });
		} else if (hour == 0 && minute == 0) {
			format = "ss" + StringUtils.substringAfter(format, "ss");
			str = StringUtils.replace(format, "ss", String.format("%0$02d", second));
		}
		return str;
	}

	/**
	 * 得到年 / 月 / 日的数组
	 */
	public static String[] getDateArray(String date) {
		if (date == null) {
			return null;
		}
		return date.split("-");
	}

	/**
	 * 得到月查询的每一天
	 */
	public static String[] getMonthEveryDay(String start_param, String end) {
		String start = start_param == null ? subMonth(end, -1) : start_param;
		String[] start_array = getDateArray(start);
		int year = Integer.valueOf(start_array[0]);
		int month = Integer.valueOf(start_array[1]);
		int day = Integer.valueOf(start_array[2]);
		int count = Integer.valueOf(getDaysBetweenTwoDates(start, end));
		String[] day_array = new String[count + 1];
		Calendar calendar = Calendar.getInstance();
		for (int i = 0; i <= count; i++) {
			calendar.clear();
			calendar.set(year, month - 1, day);
			calendar.add(Calendar.DATE, i);
			Date date = calendar.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String dateTime = sdf.format(date);
			day_array[i] = dateTime;
		}
		return day_array;
	}

	/**
	 * 得到系统日期的n天前或n天后的日期 - String
	 * 
	 * @param n
	 *            相差天数
	 * @return
	 */
	public static String newDate(int n) {
		return DateFormatUtils.format(DateUtils.addDays(new Date(), n), "yyyy-MM-dd");
	}

	/**
	 * 截取月日（04-18）
	 * 
	 * @param date
	 * @return
	 */
	public static String dateForMonthDay(String date) {
		if (date == null) {
			return null;
		}
		return date.substring(5);
	}

	/**
	 * 两个日期之间月相隔时间（2011-10-21 ~ 2011-12-20 = 2011-10-01 ~ 2011-11-30）
	 * 
	 * @param dateFrom
	 *            开始时间
	 * @param dateEnd
	 *            结束时间
	 * @return 天数
	 */
	public static int getDaysBetweenForMonth(String dateFrom, String dateEnd) {
		Date dtFrom = toDate(dateFrom, "yyyy-MM-dd");
		Date dtEnd = toDate(dateEnd, "yyyy-MM-dd");

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dtFrom);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(dtEnd);

		addMonthDayMethod(calendar, calendar2);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		return getDaysBetweenTwoDates(sdf.format(calendar.getTime()), sdf.format(calendar2.getTime()));
	}

	/**
	 * 两个日期之间月相隔月份
	 * 
	 * @param dateFrom
	 *            开始时间
	 * @param dateEnd
	 *            结束时间
	 * @return 天数
	 */
	public static int getMonthsBetween(String dateFrom, String dateEnd) {
		Date dtFrom = toDate(dateFrom, "yyyy-MM-dd");
		Date dtEnd = toDate(dateEnd, "yyyy-MM-dd");

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dtFrom);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(dtEnd);

		addMonthDayMethod(calendar, calendar2);

		int numyear = calendar2.get(Calendar.YEAR) - calendar.get(Calendar.YEAR);
		int nummonth = calendar2.get(Calendar.MONTH) - calendar.get(Calendar.MONTH);

		return numyear * 12 + nummonth;
	}

	/**
	 * 得到系统日期的n天前或n天后的日期 - String
	 * 
	 * @param format
	 *            yyyy-MM-dd
	 */
	public static String newsForHour(Calendar calendar) {
		Date date = calendar.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateTime = sdf.format(date);
		return dateTime;
	}

	/**
	 * 计算日期月份方法
	 */
	public static void addMonthDayMethod(Calendar calendar, Calendar calendar2) {
		if (calendar2.get(Calendar.DATE) >= calendar.get(Calendar.DATE)) {
			calendar2.add(Calendar.MONTH, 1);
		}
		calendar.set(Calendar.DATE, 1);
		calendar2.set(Calendar.DATE, 1);
		calendar2.add(Calendar.DATE, -1);
	}

	/**
	 * 起始日期格式(*月*日——*月*日)
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static String getDateBetween(String start, String end) {
		String month1 = start.substring(5, 7).startsWith("0") ? start.substring(6, 7) : start.substring(5, 7);
		String date1 = start.substring(8, 10).startsWith("0") ? start.substring(9, 10) : start.substring(8, 10);
		String month2 = end.substring(5, 7).startsWith("0") ? end.substring(6, 7) : end.substring(5, 7);
		String date2 = end.substring(8, 10).startsWith("0") ? end.substring(9, 10) : end.substring(8, 10);
		String result = month1 + "月" + date1 + "日" + "——" + month2 + "月" + date2 + "日";
		return result;
	}

	/**
	 * 得到当前日期所在的周（第几周）
	 * 
	 * @param date
	 * @return
	 */
	public static String getWeekString(String start) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			cal.setTime(format.parse(start));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int week = cal.get(Calendar.WEEK_OF_YEAR);
		return "第" + week + "周";
	}

	/**
	 * 日期格式化成字符串
	 * 
	 * @return
	 */
	public static String formatDateToStr(Date date, String formatType) {
		if (formatType == null) {
			formatType = "yyyy-MM-dd";
		}
		DateFormat dateFormat = new SimpleDateFormat(formatType);

		String result = dateFormat.format(date);
		return result;
	}

	/**
	 * 用截取字符串的方式获得当前月第一天
	 * 
	 * @return date String yyyy-MM-dd
	 */
	public static Date getMontFirstDay(String dateStr, String formatType) {
		String returnType = "yyyy-MM-dd";
		String resultDate = dateStr;

		if (!returnType.equalsIgnoreCase(formatType)) {
			resultDate = formatDateString(dateStr, formatType, returnType);
		}
		String result = resultDate.substring(0, 8) + "01";
		Date date = parseDate(result);

		return date;
	}

	/**
	 * 截取日期月份
	 * 
	 * @param 日期格式
	 *            2012-09-07
	 * @return string 1209
	 */
	public static String dateForYearMonth(String date) {
		if (date == null) {
			return null;
		}
		String date_sub = date.replaceAll("-", "");
		return date_sub.substring(2, 6);
	}

	/**
	 * 根据起始日期，返回期间的月份
	 * 
	 * @param arg
	 */
	public List<Integer> getBetweenMonth(String startDay, String endDay, String dateFormatType) {
		// String dateType = "yyyyMMdd";

		return null;
	}

	/**
	 * 得到指定日期 n天后的日期
	 * 
	 * @param arg
	 */
	public static String getAfterDate(String old, int n) {
		Date oldDate = toDate(old, "yyyy-MM-dd");
		return DateFormatUtils.format(DateUtils.addDays(oldDate, n), "yyyy-MM-dd");
	}

	/**************************************
	 * apache common 工具类
	 ***************************************************/

	private static String[] parsePatterns = { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy/MM/dd",
			"yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyyMMdd" };

	/**
	 * 文本时间
	 */
	public static String getDate(String month) {
		String date = month + "01";
		Date time = DateUtil.parseDate(date);
		String str = DateUtil.formatDate(time, "yyyy-MM-dd");
		String[] split = str.split("-");
		Integer a = Integer.parseInt(split[1]);
		return split[0] + "年" + a + "月";
	}

	/**
	 * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String formatDate(Date date, Object... pattern) {
		String formatDate = null;
		if (pattern != null && pattern.length > 0) {
			formatDate = DateFormatUtils.format(date, pattern[0].toString());
		} else {
			formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
		}
		return formatDate;
	}

	/**
	 * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static String formatDateTime(Date date) {
		return formatDate(date, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 得到当前时间字符串 格式（HH:mm:ss）
	 */
	public static String getTime() {
		return formatDate(new Date(), "HH:mm:ss");
	}

	/**
	 * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static String getDateTime() {
		return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 得到时间（yyyy-MM-dd）
	 */
	public static String getTruncateDateTime(String dateString) {
		return formatDate(parseDate(dateString), "yyyy-MM-dd");
	}

	/**
	 * 得到时间（yyyy-MM-dd）
	 */
	public static String getTruncateDateTime(String dateString, Integer offset) {
		Date date = DateUtils.addDays(parseDate(dateString), offset);
		return formatDate(date, "yyyy-MM-dd");
	}

	/**
	 * 得到当前年份字符串 格式（yyyy）
	 */
	public static String getYear() {
		return formatDate(new Date(), "yyyy");
	}

	/**
	 * 得到当前年份字符串 格式（yyyy）
	 */
	public static String getYear(String dateString) {
		return formatDate(parseDate(dateString), "yyyy");
	}

	/**
	 * 得到当前月份字符串 格式（MM）
	 */
	public static String getMonth() {
		return formatDate(new Date(), "MM");
	}

	/**
	 * 得到当前月份字符串 格式（MM）
	 */
	public static String getMonth(String dateString) {
		return formatDate(parseDate(dateString), "MM");
	}

	/**
	 * 得到当天字符串 格式（dd）
	 */
	public static String getDay() {
		return formatDate(new Date(), "dd");
	}

	/**
	 * 得到当天字符串 格式（dd）
	 */
	public static String getDay(String dateString) {
		return formatDate(parseDate(dateString), "dd");
	}

	/**
	 * 得到当天字符串 格式（HH）
	 */
	public static String getHour() {
		return formatDate(new Date(), "HH");
	}

	/**
	 * 得到当天字符串 格式（HH）
	 */
	public static String getHour(String dateString) {
		return formatDate(parseDate(dateString), "HH");
	}

	/**
	 * 得到当天字符串 格式（mm）
	 */
	public static String getMinute() {
		return formatDate(new Date(), "mm");
	}

	/**
	 * 得到当天字符串 格式（mm）
	 */
	public static String getMinute(String dateString) {
		return formatDate(parseDate(dateString), "mm");
	}

	/**
	 * 得到当前星期字符串 格式（E）星期几
	 */
	public static String getWeek_CN() {
		return formatDate(new Date(), "E");
	}

	/**
	 * 得到当前星期字符串 格式（E）星期几
	 */
	public static String getWeek_CN(String dateString) {
		return formatDate(parseDate(dateString), "E");
	}

	/**
	 * 日期型字符串转化为日期 格式 { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm",
	 * "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm" }
	 */
	public static Date parseDate(Object str) {
		if (str == null) {
			return null;
		}
		try {
			return DateUtils.parseDate(str.toString(), parsePatterns);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * 获取过去的天数
	 * 
	 * @param date
	 * @return
	 */
	public static long pastDays(Date date) {
		long t = new Date().getTime() - date.getTime();
		return t / (24 * 60 * 60 * 1000);
	}

	public static Date getDateStart(Date date) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			date = sdf.parse(formatDate(date, "yyyy-MM-dd") + " 00:00:00");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static Date getDateEnd(Date date) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			date = sdf.parse(formatDate(date, "yyyy-MM-dd") + " 23:59:59");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 将日期格式为yyyyMM的字符串转换成yyyy年MM月
	 */
	public static String formatStrTp(String date) {

		String str1 = date.substring(0, 4);
		String str2 = date.substring(4, 6);
		String str3 = str1 + "年" + Integer.parseInt(str2) + "月";
		return str3;
	}

	/**
	 * 根据年月获取该月的天数 格式yyyyMM字符串
	 */
	public static int getDaysByYearMonth(String date) {
		int year = Integer.parseInt(date.substring(0, 4));
		int month = Integer.parseInt(date.substring(5, 6));
		Calendar a = Calendar.getInstance();
		a.set(Calendar.YEAR, year);
		a.set(Calendar.MONTH, month - 1);
		a.set(Calendar.DATE, 1);
		a.roll(Calendar.DATE, -1);
		int maxDate = a.get(Calendar.DATE);
		return maxDate;
	}

	/**
	 * 获取格式为"201707"字符串格式上一个月"2017年6月"
	 */
	public static String getLastMonth(String date) {
		int year = Integer.parseInt(date.substring(0, 4));
		int month = Integer.parseInt(date.substring(4, 6));
		String yearStr = "";
		String monthStr = "";
		String lastMonth = "";
		// 如果是1月，上个月是上年12月
		if (month == 1) {
			yearStr = (year - 1) + "";
			monthStr = "12";
		} else {
			yearStr = year + "";
			monthStr = (month - 1) + "";

		}
		lastMonth = yearStr + "年" + monthStr + "月";
		return lastMonth;
	}



	/**
	 * 时间参数到月份截至,根据时间参数获得具体的开始时间
	 * 
	 * @param month
	 * @author wyc
	 * @return
	 */
	public static String getStartDay(String month) {
		String startDay = month + "01";
		return startDay;
	}

	public static String getEndDay(String month) {
		String startDay = month + "01";
		Date start = DateUtil.parseDate(startDay);
		Date end_temp = DateUtils.addMonths(start, 1);
		Date endTime = DateUtils.addDays(end_temp, -1);
		String endDay = DateUtil.dateToString(endTime, "yyyyMMdd");
		return endDay;
	}

	/**
	 * 完整的文本时间完整的
	 */
	public static String getCompleteDate(String month) {
		String date = month + "01";
		Date time = DateUtil.parseDate(date);
		String str = DateUtil.formatDate(time, "yyyy-MM-dd");
		return str;
	}

	/**
	 * 根据年和月，查询当月有几天
	 */
	public static int getDayByYearAndMonth(int year, int month) {
		int days = 0;
		if (month != 2) {
			switch (month) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				days = 31;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				days = 30;
			}
		} else {
			// 闰年
			if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
				days = 29;
			else
				days = 28;
		}
		return days;
	}


	/**
	 * 用截取字符串的方式获得当前月第一天
	 * 
	 * @return date String yyyy-MM-dd
	 */
	public static Date getMontFirstDay(Date date) {
		String dateStr = dateToString(date, "yyyy-MM-dd");

		return getMontFirstDay(dateStr, "yyyy-MM-dd");
	}


	/**
	 * 文本时间--文本接口
	 * 
	 * @param month
	 * @author wyc
	 * @return
	 */
	public static String getTextDate(String month) {
		String date = month + "01";
		Date time = DateUtil.parseDate(date);
		String str = DateUtil.formatDate(time, "yyyy-MM-dd");
		String[] split = str.split("-");
		return split[0] + "年" + split[1] + "月";
	}

	/**
	 * 获取明年的年份
	 * 
	 * @return
	 * @author wyc
	 */
	public static String getLastYear() {

		Calendar calendar = Calendar.getInstance();
		Date date = new Date(System.currentTimeMillis());
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, -1);
		date = calendar.getTime();
		String dateStr = DateUtil.formatDate(date, "yyyyMMdd");
		String lastYear = dateStr.substring(0, 4);

		return lastYear;
	}


	/**
	 * 根据上下半年度获取 起始日期
	 * 
	 * @param year
	 * @return
	 * @author wyc
	 */
	public static String StartDayByYear(Integer halfyear) {
		// 获取当前年份
		Calendar calendar = Calendar.getInstance();
		Date date = new Date(System.currentTimeMillis());
		calendar.setTime(date);
		date = calendar.getTime();
		String dateStr = DateUtil.formatDate(date, "yyyyMMdd");
		String year = dateStr.substring(0, 4);
		String startDay = "";
		if (halfyear == 1) {
			startDay = year + "0101";

		} else if (halfyear == 2) {
			startDay = year + "0615";
		}
		return startDay;
	}

	/**
	 * 根据上下半年度获取 截至日期
	 * 
	 * @param year
	 * @return
	 * @author wyc
	 */
	public static String endDayByYear(Integer halfyear) {
		// 获取当前年份
		Calendar calendar = Calendar.getInstance();
		Date date = new Date(System.currentTimeMillis());
		calendar.setTime(date);
		date = calendar.getTime();
		String dateStr = DateUtil.formatDate(date, "yyyyMMdd");
		String year = dateStr.substring(0, 4);
		String endDay = "";
		if (halfyear == 1) {

			endDay = year + "0615";
		} else if (halfyear == 2) {

			endDay = year + "1231";
		}
		return endDay;
	}

	/**
	 * 根据上下半年获取文本时间
	 * 
	 * @author wyc
	 * @param halfYear
	 * @return
	 */
	public static String getDateByYear(Integer halfYear) {
		// 获取当前年份
		Calendar calendar = Calendar.getInstance();
		Date date = new Date(System.currentTimeMillis());
		calendar.setTime(date);
		date = calendar.getTime();
		String dateStr = DateUtil.formatDate(date, "yyyyMMdd");
		String year = dateStr.substring(0, 4);
		if (halfYear == 1) {

			return year + "1月1日-6月15日";
		} else {

			return year + "6月15日-12月31日";
		}
	}
	/**
	 * 根据开始和结束日期得到之间所有日期集合
	 * 
	 * @author wx
	 * @param startTime,endTime
	 * @return
	 */
	public static List<String> getDaysList(String startTime,String endTime) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(startTime));
		List<String> list=new ArrayList<String>();
		for (long d = cal.getTimeInMillis(); d <= sdf.parse(endTime).getTime(); d = get_D_Plaus_1(cal)) {
			list.add(sdf.format(d));
		}
		list.size();
		return list;
	}
	private static long get_D_Plaus_1(Calendar c) {  
        c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + 1);  
        return c.getTimeInMillis();  
    } 
}
