package com.terren.common.util;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

/**
 * Json Util
 * @author Administrator
 *
 */
public class JsonUtil {
	
	/**
	 * 分页 
	 * @param data
	 * @param recordsTotal
	 * @return
	 */
	public static String pagingJson(List<?> data){
		return pagingJson(data,data.size(),null,null);
	}
	/**
	 * 分页 2，添加pageSize常量,用recordsFiltered替代
	 * @param data
	 * @return
	 */
	
	public static String pagingJson(List<?> data,Integer pageSize){
		return pagingJson(data,data.size(),pageSize,null);
	}
	
	/**
	 * 得到对象的JSON
	 * @param obj
	 * @return
	 */
	public static String toJSONString(Object obj) {
		return new Gson().toJson(obj);
	}
	
	/**
	 * amazeui分页
	 * @param data
	 * @param recordsTotal
	 * @param draw
	 * @return
	 */
	public static String pagingJson(List<?> data,Integer recordsTotal,Integer draw){
		
		return pagingJson(data,recordsTotal,recordsTotal,draw);
	}
	/**
	 * 
	 * @param data
	 * @param recordsTotal
	 * @param recordsFiltered
	 * @param draw
	 * @return
	 */
	public static String pagingJson(List<?> data,Integer recordsTotal,Integer recordsFiltered,Integer draw){
		AmazeuiData store = new AmazeuiData();
		store.setData(data);
		store.setRecordsTotal(recordsTotal);
		store.setDraw(draw);
		store.setRecordsFiltered(recordsFiltered);
		String json = new Gson().toJson(store);
		return json;
	}
	
	
	/**
	 * 返回ajax执行结果
	 * @param isSuccess
	 * @param msg
	 * @return
	 */
	public static String messageJson(boolean isSuccess,String msg){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", isSuccess);
		resultMap.put("msg", msg);
		String json = new Gson().toJson(resultMap);
		return json;
	}
	/**
	 * json字符串转list
	 * @param json
	 * @param clazz
	 * @return
	 */
	public static <T> List<T> fromJsonList(String json, Class<T> clazz) {
		List<T> lst = new ArrayList<T>();
		try {
			JsonArray array = new JsonParser().parse(json).getAsJsonArray();
			for (final JsonElement elem : array) {
				lst.add(new Gson().fromJson(elem, clazz));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lst;
	}
	/**
	 * 获取json数据中某个节点下的json
	 * @param data
	 * @param jsonNode
	 * @return
	 */
	public static String filterJson(String data,String jsonNode){
		String filterData = null;
		Gson gson = new Gson();
		Type type = new TypeToken<Map<String, Object>>(){}.getType();
		Map<String, Object> myMap = gson.fromJson(data, type);
		
		filterData = gson.toJson(myMap.get(jsonNode));
		return filterData;
	}
	
	public static String xml2Json(String xml) throws Exception{
		Document doc = DocumentHelper.parseText(xml);
		Element root = doc.getRootElement();
		return new Gson().toJson(root.getData());
	}
}
class AmazeuiData {
	private Integer recordsFiltered;
	private Integer draw;
	private Integer recordsTotal;
	private List<?> data;
	public Integer getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(Integer recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	public Integer getDraw() {
		return draw;
	}
	public void setDraw(Integer draw) {
		this.draw = draw;
	}
	public Integer getRecordsTotal() {
		return recordsTotal;
	}
	public void setRecordsTotal(Integer recordsTotal) {
		this.recordsTotal = recordsTotal;
	}
	public List<?> getData() {
		return data;
	}
	public void setData(List<?> data) {
		this.data = data;
	}
}

