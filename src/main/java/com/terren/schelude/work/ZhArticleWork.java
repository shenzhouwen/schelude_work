package com.terren.schelude.work;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.collections.CollectionUtils;

import com.terren.common.entity.ZhArticle;
import com.terren.common.util.CommonUtil;
import com.terren.config.SysConfig;
import com.terren.db.service.ZhArticleService;

public class ZhArticleWork extends BasicWork implements Runnable {
	
    public ZhArticleWork(CountDownLatch countDownLatch){
    	super();
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
    	ZhArticleService zhArticleService = CommonUtil.getBean(ZhArticleService.class);
        try {
        	
            log.info(Thread.currentThread().getName() + "启动@" + System.currentTimeMillis());
            while(true) {
            	ZhArticle art = new ZhArticle();
            	art.setLimit(SysConfig.LIMIT);
            	List<ZhArticle> list = zhArticleService.getArticleList(art);
                if(CollectionUtils.isEmpty(list)) {
            		break;
            	}
        		List<ZhArticle> dealList = new ArrayList<ZhArticle>();
        		int total = list.size();
        		int i = 1;
        		for(ZhArticle a : list) {
        			//调接口
        			log.info("当前第 "+i+"篇知乎文章正在调用接口,本轮共计"+total+"篇知乎文章");
        			i++;
        			List<String> keywords = zhArticleService.getKeywords(a);
        			if(CollectionUtils.isNotEmpty(keywords)) {
        				Boolean flag = CommonUtil.keywords(a.getTopicContent(),keywords);
            			if(flag) {
            				Integer score = getScore(a.getTopicContent());
                			if(score !=-1 && findKeyword(a.getTopicContent(), set)) {
                				a.setOpinion_analysis(score);
                				StringBuilder sb = new StringBuilder();
                				int m = keywords.size();
                				for(int k=1;k<=m;k++) {
                					if(k==m) {
                						sb.append(keywords.get(k-1));	
                					}else {
                						sb.append(keywords.get(k-1)).append(",");
                					}
                					
                				}
                				a.setKeyword(sb.toString());
                				dealList.add(a);
                			}
            			}
        			}
        		}
        		if(CollectionUtils.isNotEmpty(dealList)) {
        			opinionAnalysisService.saveZhArticles(dealList);
        		}
        		zhArticleService.updateDoneStatus(list);
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
        	if(countDownLatch !=null)
        		countDownLatch.countDown();
        	log.info("知乎文章调用接口完成......");
		} 
    }
}
