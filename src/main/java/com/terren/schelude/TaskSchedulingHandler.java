package com.terren.schelude;

public interface TaskSchedulingHandler {
	
	public void startSchedule();
	
	/**
	 * 每天执行的任务 0点
	 * @throws Exception
	 */
	public void dailyTask()throws Exception;
	
	
}
