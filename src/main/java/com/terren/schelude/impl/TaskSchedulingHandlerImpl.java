package com.terren.schelude.impl;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.terren.common.util.LogUtil;
import com.terren.schelude.TaskSchedulingHandler;
import com.terren.schelude.work.ArticleWork;
import com.terren.schelude.work.WxArticleWork;
import com.terren.schelude.work.ZhArticleWork;


/**
 * 定时任务相关配置在：CustomSchedulingConfigurer
 * 
 */


@Component
public class TaskSchedulingHandlerImpl implements TaskSchedulingHandler {
	Logger log = LogUtil.logger;
	@Override
	public void startSchedule() {
		log.info("Scheduled Task Start...");
	}
	/**
	 * 每天0点执行一次 
	 */
	@Override
	//@Scheduled(cron="0 0 0 * * ?") 
	public void dailyTask() throws Exception {
		CountDownLatch latch = new CountDownLatch(3);
		new Thread(new ArticleWork(latch)).start();
		new Thread(new WxArticleWork(latch)).start();
		new Thread(new ZhArticleWork(latch)).start();
		latch.await();
	}

}
