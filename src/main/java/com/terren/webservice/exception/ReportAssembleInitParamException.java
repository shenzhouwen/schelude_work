package com.terren.webservice.exception;

public class ReportAssembleInitParamException extends Exception{

	/**
	 * 生成jrxml异常
	 */
	private static final long serialVersionUID = 2250572748169324711L;
	public ReportAssembleInitParamException(String message){
		super(message);
	}
	public ReportAssembleInitParamException(String message,Throwable cause){
		super(message,cause);
	}
	public ReportAssembleInitParamException(Throwable cause){
		super(cause);
	}
}
