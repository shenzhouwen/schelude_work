package com.terren.webservice.exception;

public class ReportAssembleException extends Exception{

	/**
	 * 生成jrxml异常
	 */
	private static final long serialVersionUID = 2250572748169324711L;
	public ReportAssembleException(String message){
		super(message);
	}
	public ReportAssembleException(String message,Throwable cause){
		super(message,cause);
	}
	public ReportAssembleException(Throwable cause){
		super(cause);
	}
}
