package com.terren.webservice.exception;

public class ReportExportException extends Exception{

	/**
	 * 导出异常
	 */
	private static final long serialVersionUID = 2250372748169324711L;
	public ReportExportException(String message){
		super(message);
	}
	public ReportExportException(String message,Throwable cause){
		super(message,cause);
	}
	public ReportExportException(Throwable cause){
		super(cause);
	}
}
