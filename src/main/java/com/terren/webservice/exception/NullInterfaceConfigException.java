package com.terren.webservice.exception;

public class NullInterfaceConfigException extends Exception{

	/**
	 * 空的接口配置异常
	 */
	private static final long serialVersionUID = 2250572748169324711L;
	public NullInterfaceConfigException(String message){
		super(message);
	}
	public NullInterfaceConfigException(String message,Throwable cause){
		super(message,cause);
	}
	public NullInterfaceConfigException(Throwable cause){
		super(cause);
	}

}
