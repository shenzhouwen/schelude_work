package com.terren.webservice.exception;

public class ReportCompileException extends Exception{

	/**
	 * 编译异常
	 */
	private static final long serialVersionUID = 2250572748269324711L;
	public ReportCompileException(String message){
		super(message);
	}
	public ReportCompileException(String message,Throwable cause){
		super(message,cause);
	}
	public ReportCompileException(Throwable cause){
		super(cause);
	}
}
