package com.terren.webservice.exception;

public class UrlNullException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2831133117877653456L;
	/**
	 * 请求错误异常
	 */
	public UrlNullException(String message){
		super(message);
	}
	public UrlNullException(String message,Throwable cause){
		super(message,cause);
	}
	public UrlNullException(Throwable cause){
		super(cause);
	}
}
