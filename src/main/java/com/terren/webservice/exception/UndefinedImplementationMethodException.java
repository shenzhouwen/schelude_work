package com.terren.webservice.exception;

public class UndefinedImplementationMethodException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2831233117877653436L;
	/**
	 * 请求错误异常
	 */
	public UndefinedImplementationMethodException(String message){
		super(message);
	}
	public UndefinedImplementationMethodException(String message,Throwable cause){
		super(message,cause);
	}
	public UndefinedImplementationMethodException(Throwable cause){
		super(cause);
	}
}
