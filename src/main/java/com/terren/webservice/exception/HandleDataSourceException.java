package com.terren.webservice.exception;

public class HandleDataSourceException extends Exception{

	/**
	 * 请求错误异常
	 */
	private static final long serialVersionUID = 2350572748169324713L;
	public HandleDataSourceException(String message){
		super(message);
	}
	public HandleDataSourceException(String message,Throwable cause){
		super(message,cause);
	}
	public HandleDataSourceException(Throwable cause){
		super(cause);
	}
}
