package com.terren.webservice.exception;

public class ResponseDataNullException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2831133117877653436L;
	/**
	 * 请求错误异常
	 */
	public ResponseDataNullException(String message){
		super(message);
	}
	public ResponseDataNullException(String message,Throwable cause){
		super(message,cause);
	}
	public ResponseDataNullException(Throwable cause){
		super(cause);
	}
}