package com.terren.webservice.common.entity;

public enum WebserviceType {
	RESTFUL("restful"),
	SOAP("soap");
	private String type;
	WebserviceType(String type){
		this.type = type;
	}
	public static WebserviceType valueOfType(String type) {
		for (WebserviceType servicetype : values()) {
			if (servicetype.type.equalsIgnoreCase(type)) {
				return servicetype;
			}
		}
		return RESTFUL;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public String getType(){
		return this.type;
	}
}
