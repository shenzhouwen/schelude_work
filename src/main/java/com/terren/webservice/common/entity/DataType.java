package com.terren.webservice.common.entity;

public enum DataType {
	XML(1,"xml"),
	JSON(0,"json");
	private String typeName;
	private Integer typeValue; 
	DataType(Integer typeValue,String typeName){
		this.typeName = typeName;
		this.typeValue = typeValue;
	}
	public static DataType nameOfType(String typeName) {
		for (DataType datatype : values()) {
			if (datatype.typeName.equalsIgnoreCase(typeName)) {
				return datatype;
			}
		}
		return JSON;
	}
	
	public static DataType valueOfType(Integer typeValue) {
		for (DataType datatype : values()) {
			if (datatype.typeValue == typeValue) {
				return datatype;
			}
		}
		return JSON;
	}
	
	public Integer getTypeValue() {
		return typeValue;
	}
	public void setTypeValue(Integer typeValue) {
		this.typeValue = typeValue;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getTypeName(){
		return this.typeName;
	}
}
