package com.terren;

import org.apache.log4j.Logger;

import com.terren.common.util.CommonUtil;
import com.terren.common.util.LogUtil;
import com.terren.schelude.TaskSchedulingHandler;

public class Main {
	public static void main(String[] args) throws Exception {
		LogUtil.initLogger();
		Logger logger = LogUtil.getLogger(Main.class);
		//SysConfig.SCHEDULED_RUN = true;
		TaskSchedulingHandler handler = CommonUtil.getBean(TaskSchedulingHandler.class);
		while (true) {
			try {
				handler.startSchedule();
				handler.dailyTask();
			
			} catch (Exception e) {
				logger.error("runTask error", e);
			} finally {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					logger.error("sleep runTask error", e1);
				}
			}
		}
	}

}
