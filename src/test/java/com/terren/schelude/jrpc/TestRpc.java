package com.terren.schelude.jrpc;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;


public class TestRpc {
	@Test
	public void rpcTest() throws Throwable {
		JsonRpcHttpClient client_sent = new JsonRpcHttpClient(new URL("http://finrnn.first-call.cn:6060"));
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Rpc-Type", "shop");
		client_sent.setHeaders(headers);
		String text9 = "这家公司很不错";
		String score9 = client_sent.invoke("getSentiment",new Object[]{text9},String.class);   //长文本

		String score8 = client_sent.invoke("getSentimentRnn",new Object[]{text9},String.class); //短文本
		System.out.println(score8 + " / " + score9);
	}
}
